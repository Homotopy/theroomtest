# frozen_string_literal: true

# Migration class for tracks
class CreateTracks < ActiveRecord::Migration[6.1]
  def change
    create_table :tracks do |t|
      t.string :private_id
      t.string :artist_name
      t.string :name
      t.string :genres, array: true, default: []
      t.string :link

      t.timestamps
    end
    add_attachment :tracks, :picture
  end
end
