# frozen_string_literal: true

# Add rating colulmn to Track table
class AddRatingToTracks < ActiveRecord::Migration[6.1]
  def change
    add_column :tracks, :rating, :integer
  end
end
