# frozen_string_literal: true

# Add index to private_id tracks
class AddIndexToTracks < ActiveRecord::Migration[6.1]
  def change
    add_index :tracks, :private_id, unique: true
  end
end
