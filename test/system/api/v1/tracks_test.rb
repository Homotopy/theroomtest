# frozen_string_literal: true

require 'application_system_test_case'

class Api::V1::TracksTest < ApplicationSystemTestCase
  setup do
    @api_v1_track = api_v1_tracks(:one)
  end

  test 'visiting the index' do
    visit api_v1_tracks_url
    assert_selector 'h1', text: 'Api/V1/Tracks'
  end

  test 'creating a Track' do
    visit api_v1_tracks_url
    click_on 'New Api/V1/Track'

    click_on 'Create Track'

    assert_text 'Track was successfully created'
    click_on 'Back'
  end

  test 'updating a Track' do
    visit api_v1_tracks_url
    click_on 'Edit', match: :first

    click_on 'Update Track'

    assert_text 'Track was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Track' do
    visit api_v1_tracks_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Track was successfully destroyed'
  end
end
