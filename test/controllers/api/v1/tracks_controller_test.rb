# frozen_string_literal: true

require 'test_helper'

class Api::V1::TracksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @api_v1_track = api_v1_tracks(:one)
  end

  test 'should get index' do
    get api_v1_tracks_url
    assert_response :success
  end

  test 'should get new' do
    get new_api_v1_track_url
    assert_response :success
  end

  test 'should create api_v1_track' do
    assert_difference('Api::V1::Track.count') do
      post api_v1_tracks_url, params: { api_v1_track: {} }
    end

    assert_redirected_to api_v1_track_url(Api::V1::Track.last)
  end

  test 'should show api_v1_track' do
    get api_v1_track_url(@api_v1_track)
    assert_response :success
  end

  test 'should get edit' do
    get edit_api_v1_track_url(@api_v1_track)
    assert_response :success
  end

  test 'should update api_v1_track' do
    patch api_v1_track_url(@api_v1_track), params: { api_v1_track: {} }
    assert_redirected_to api_v1_track_url(@api_v1_track)
  end

  test 'should destroy api_v1_track' do
    assert_difference('Api::V1::Track.count', -1) do
      delete api_v1_track_url(@api_v1_track)
    end

    assert_redirected_to api_v1_tracks_url
  end
end
