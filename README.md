# TheRoom

TheRoom is a ruby on rails website allowing you to rate you favorite songs.

## Installation
Use the package manager [docker](https://docs.docker.com/engine/install/) to install TheRoom.
You also need the database [postgres](https://www.postgresql.org/download/) working.
You need access to spotify api [spotify](https://developer.spotify.com/)
## Variables
```
EDITOR=vim rails credentials:edit --environment development
SPOTIFY_CLIENT_ID: ********
SPOTIFY_CLIENT_SECRET: ********
```

### Run the app only, from docker
```bash
docker build -t prebuild -f prebuild.Dockerfile .                   
docker build -t app -f Dockerfile --build-arg PREBUILD_NAME=prebuild --build-arg RAILS_ENV=development --build-arg PORT=3000 .
```
Or
### Run everything with docker-compose
```bash
docker build -t prebuild -f prebuild.Dockerfile . 
docker-compose build
```
## Usage

```bash
docker run -it --rm app:latest -p 3000
bundle exec rake db:create
bundle exec rake db:migrate
```
Also
```bash
docker-compose up
docker-compose exec build bash
bundle exec rake db:create
bundle exec rake db:migrate
```
Then go checkout the app at [theroom](localhost:3000)

## License

[MIT](https://choosealicense.com/licenses/mit/)