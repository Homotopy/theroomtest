# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Track, type: :model do
  describe 'class methods' do
    context 'when calls ingest' do
      let(:spotify_tracks) do
        JSON.parse(File.read('spec/fixtures/songs_results.json'))['tracks']['items'].map do |json_track|
          RSpotify::Track.new(json_track)
        end
      end

      it 'create list of Track' do
        expect(described_class.ingest(spotify_tracks)).to eq(JSON.parse(File.read('spec/fixtures/tracks.json')))
      end
    end

    context 'when call add_pictures' do
      let(:tracks_fake) do
        JSON.parse(File.read('spec/fixtures/tracks.json'))
      end

      it 'change file path to file name' do
        allow(described_class).to receive(:save_picture).with(anything, anything)

        expect(described_class.add_pictures(tracks_fake).map do |track|
                 track['picture'].class.name
               end.uniq).to eq(['File'])
      end
    end

    context 'when call save_picture' do
      it 'create new file' do
        path = 'tmp/images/fake_file'
        allow(URI).to receive(:open).with(anything).and_yield(File.open('spec/fixtures/track.json'))
        described_class.save_picture('fake_url', path)
        expect(File.exist?(path)).to be true
      end
    end
  end

  describe 'instance methods' do
    context 'when calls render' do
      let :track_result do
        {
          artist_name: 'singer',
          genres: [],
          link: 'www.example.com',
          name: 'good song',
          picture: :picture,
          private_id: '01'
        }
      end

      it 'format track into hash' do
        track = build :track
        expect(track.render).to eq(track_result)
      end
    end
  end
end
