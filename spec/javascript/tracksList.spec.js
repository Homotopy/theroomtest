
import Trackslist from 'TheRoom/components/trackslist/main.jsx'
import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom'
import data from '../fixtures/tracks.json';
describe('test TracksList component', () => {
    test('all attributes are filled', () => {
        render(<MemoryRouter><Trackslist tracks={data} /></MemoryRouter>);

        expect(screen.getAllByAltText("track").length).toEqual(20);
        expect(screen.getAllByText("Set Fire to the Rain").length).toBeGreaterThan(0);
        expect(screen.getAllByText("By Adele").length).toBeGreaterThan(0);
    });
});