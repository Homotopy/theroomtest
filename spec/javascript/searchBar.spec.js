
import SearchBar from 'TheRoom/components/searchbar/main.jsx'
import React from 'react';
import { render, screen, within } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom'
import data from '../fixtures/tracks.json';
describe('test TracksList component', () => {
    test('have a search bar with an input', () => {
        render(<MemoryRouter><SearchBar setTracksValue={() => ("")} /></MemoryRouter>);

        expect(within(screen.getByRole("search")).getByText("Search")).toBeTruthy()

    });
});