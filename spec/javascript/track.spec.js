
import Track from 'TheRoom/components/track/main.jsx'
import React from 'react';
import { render, screen } from '@testing-library/react';
import data from '../fixtures/track.json';
describe('test Track components', () => {
    test('all attributes are filled', () => {
        render(<><div name="csrf-token">test</div><Track track={data} /></>);

        expect(screen.getAllByText("go listen").length).toBeGreaterThan(0);
        expect(screen.getAllByText("Set Fire to the Rain").length).toBeGreaterThan(0);
        expect(screen.getAllByText("By Adele").length).toBeGreaterThan(0);
    });
});