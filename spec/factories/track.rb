# frozen_string_literal: true

FactoryBot.define do
  factory :track do
    name { 'good song' }
    private_id { '01' }
    link { 'www.example.com' }
    artist_name { 'singer' }
    picture { File.new(Rails.root.join('spec/fixtures/picture.png')) }
  end
end
