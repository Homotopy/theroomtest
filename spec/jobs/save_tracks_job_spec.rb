# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SaveTracksJob, type: :job do
  describe 'instance methods' do
    context 'when perform_async called' do
      let(:tracks_fake) do
        JSON.parse(File.read('spec/fixtures/tracks.json'))
      end

      it 'create tracks' do
        allow(Track).to receive(:save_picture)
        FileUtils.copy_entry 'spec/fixtures/images', 'tmp/images'
        expect { described_class.new.perform(tracks_fake) }.to(change(Track, :count))
      end
    end
  end
end
