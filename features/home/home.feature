@javascript
Feature: About home page
  Scenario: Visiting the home page try the search bar and visit a link
    Given I am on the "home" page
    When Tracks are not loaded
    When I write in the search bar
    Then Tracks are loaded
    Then I click on a link
    And I am on track/:id

  Scenario: Visiting the track id and change rating
    Given I am on the track page
    Given Track as no star
    Then I change the number
    Then I refresh the page
    And The number of star is the same