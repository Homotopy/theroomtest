# frozen_string_literal: true

Given('I am on the {string} page') do |_string|
  visit('/')
end

When('I write in the search bar') do
  fill_in 'searchInput', with: 'Set Fire to the Rain'
end

Then('Tracks are not loaded') do
  expect(page.all('.card').count).to eq(0)
end

Then('Tracks are loaded') do
  expect(page).to have_content('Fire to the Rain - Original Pitch')
  expect(page.all('.card').count).to eq(20)
end

Then('I click on a link') do
  a = find_all('a')[0]
  @link = a['href']
  a.click
end

Then('I am on track\/:id') do
  expect(page).to have_current_path(@link)
end

Then('It works') do
  expect(page.body).to be_truthy
end

Given('I am on the track page') do
  @track = create :track
  visit("tracks/#{@track.private_id}")
end

Given('Track as no star') do
  page.all('div.Th5IfOI0p7fhJIItl2S_ > label').each do |label|
    expect(label.style('color')['color']).to eq('rgba(34, 34, 34, 1)')
  end

  expect(page.all('div.Th5IfOI0p7fhJIItl2S_ > label').count).to eq(5)
end

Then('I change the number') do
  page.all('div.Th5IfOI0p7fhJIItl2S_ > label')[3].click
end

Then('I refresh the page') do
  visit("tracks/#{@track.private_id}")
end

Then('The number of star is the same') do
  page.all('div.Th5IfOI0p7fhJIItl2S_ > label').each.with_index do |label, index|
    if index >= 3
      expect(label.style('color')['color']).to  eq('rgba(255, 202, 8, 1)')
    else
      expect(label.style('color')['color']).to  eq('rgba(34, 34, 34, 1)')
    end
    expect(page.all('div.Th5IfOI0p7fhJIItl2S_ > label').count).to eq(5)
  end

  expect(Track.first.rating).to eq(2)
end
