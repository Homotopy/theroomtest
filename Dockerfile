ARG PREBUILD_NAME=prebuild
FROM $PREBUILD_NAME
LABEL author=homotopy

# Project setup
WORKDIR /app
COPY Gemfile* /app/
RUN bundle install --frozen --jobs $(nproc)

ADD package.json .
RUN yarn install --frozen-lockfile
COPY . /app/


ARG RAILS_ENV=development
ENV RAILS_ENV=$RAILS_ENV
RUN NODE_OPTIONS=--openssl-legacy-provider bundle exec rails webpacker:compile

ARG PORT=3000

EXPOSE $PORT

ENTRYPOINT ["rails", "server", "-b", "0.0.0.0"]