# frozen_string_literal: true

require 'rspotify'

RSpotify.authenticate(Rails.application.credentials.SPOTIFY_CLIENT_ID, Rails.application.credentials.SPOTIFY_CLIENT_SECRET)
