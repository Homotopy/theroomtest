# frozen_string_literal: true

Rails.application.routes.draw do
  root "api/v1/tracks#index"
  get '/tracks', to: 'api/v1/tracks#index'
  get '/tracks/:private_id', to: 'api/v1/tracks#show'
  put '/tracks/:private_id', to: 'api/v1/tracks#update'
  post '/tracks', to: 'api/v1/tracks#create'

  namespace :api do
    namespace :v1 do
      resources :tracks, param: :private_id
    end
  end

  get 'welcome/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
