import PropTypes from 'prop-types';
import React from 'react';
import "stylesheets/application.scss";
import ListItems from 'bundles/TheRoom/components/trackslist/main.jsx';

const Index = (props) => {
    return (
        <>
            <br />
            <ListItems tracks={props.tracks} />
        </>
    );
};

Index.propTypes = {
    tracks: PropTypes.array.isRequired, // this is passed from the Rails view
};

export default Index;