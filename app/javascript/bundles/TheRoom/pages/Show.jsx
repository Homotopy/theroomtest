import PropTypes from 'prop-types';
import { useLocation } from "react-router-dom";
require("bootstrap");
import "bundles/TheRoom/components/searchbar/input.module.css";
import React from 'react';
import Track from 'bundles/TheRoom/components/track/main.jsx';
document.addEventListener("turbolinks:load", function () {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();
    });
});

const Show = (props) => {
    return (
        <div className="row justify-content-center">
            <div className="col-4">
                <Track track={props.track || useLocation().state.track} />
            </div>
        </div>
    );
};

Show.propTypes = {
    track: PropTypes.object.isRequired, // this is passed from the Rails view
};
export default Show;