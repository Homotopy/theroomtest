import React from 'react';
import { Route, Routes } from 'react-router-dom';

import Show from 'bundles/TheRoom/pages/Show.jsx';
import Index from 'bundles/TheRoom/pages/Index.jsx';
const Routing = (props) => (
    <Routes>
        <Route exact path="/" element={<Index {...props} />} />
        <Route exact path="/tracks" element={<Index {...props} />} />
        <Route exact path="/tracks/:id" element={<Show {...props} />} />
    </Routes>
);

export default Routing;