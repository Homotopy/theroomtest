import PropTypes from 'prop-types';
import React, { useState, useEffect, useRef } from 'react';

import style from 'bundles/TheRoom/components/track/main.module.css';
import axios from 'axios';

const track = (props) => {
    const rating = props.track.rating
    const [ratingValue, setRatingValue] = useState(rating);
    const didMount = useRef(false);

    function changeRatingValue(event) {
        setRatingValue(event.target.value)
    }

    useEffect(() => {
        const token = document.querySelector('[name=csrf-token]') ? document.querySelector('[name=csrf-token]').content : ""

        const config = { headers: { 'Content-Type': 'application/json', "Accept": "application/json" } };
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token
        if (didMount.current) {
            axios.put('/api/v1/tracks/' + props.track.private_id, {
                rating: ratingValue
            }, config)
                .then(function (response) {
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                })

        } else {
            didMount.current = true
            axios.get('/api/v1/tracks/' + props.track.private_id, config)
                .then(function (response) {
                    setRatingValue(response.data.track.rating);
                })
                .catch(function (error) {
                    console.log(error);
                })
        }
    }, [ratingValue])
    return (
        <div className="card border-dark text-bg-dark h-100">
            <img src={props.track.picture_path || props.track.picture} className="card-img-top" />
            <div className="card-body">
                <h5 className="card-title">{props.track.name}</h5>
                <p className="card-text">By {props.track.artist_name}</p>
                <p className="card-text">{props.track.genres}</p>
                <br />

                <div className="container">
                    <div className={style.starrating + " " + style.risingstar + " d-flex justify-content-center flex-row-reverse"}>
                        <input type="radio" id="star5" name="rating" value="5" checked={5 == ratingValue} onChange={changeRatingValue} /><label htmlFor="star5" title="5 star"></label>
                        <input type="radio" id="star4" name="rating" value="4" checked={4 == ratingValue} onChange={changeRatingValue} /><label htmlFor="star4" title="4 star"></label>
                        <input type="radio" id="star3" name="rating" value="3" checked={3 == ratingValue} onChange={changeRatingValue} /><label htmlFor="star3" title="3 star"></label>
                        <input type="radio" id="star2" name="rating" value="2" checked={2 == ratingValue} onChange={changeRatingValue} /><label htmlFor="star2" title="2 star"></label>
                        <input type="radio" id="star1" name="rating" value="1" checked={1 == ratingValue} onChange={changeRatingValue} /><label htmlFor="star1" title="1 star"></label>
                    </div>
                </div>

                <a className="link-light link-offset-2 link-offset-3-hover link-underline link-underline-opacity-0 link-underline-opacity-75-hover" href={props.track.link}>
                    <p className="w-100 btn btn-dark">
                        go listen
                    </p>
                </a>
            </div>
        </div>
    );
};

track.propTypes = {
    track: PropTypes.object.isRequired,
};

export default track;