import PropTypes from 'prop-types';
import React from 'react';

const track = (props) => {
    return (
        <div className="card border-dark text-bg-dark h-100">
            <img src={props.track.picture} className="card-img-top" alt="track" />
            <div className="card-body">
                <h5 className="card-title">{props.track.name}</h5>
                <p className="card-text">By {props.track.artist_name}</p>
            </div>
        </div>
    );
};

track.propTypes = {
    track: PropTypes.object.isRequired,
};

export default track;