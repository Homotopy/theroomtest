import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import React from 'react';
import Track from './track.jsx';

const TracksList = (props) => {
    if (!!props.tracks) {
        return (
            <div className="row row-cols-3 row-cols-md-5 g-4">
                {
                    props.tracks.map((track, i) => <li className="col" key={i}><Link to={"/tracks/" + track.private_id} state={{ track: track }} ><Track track={track} /></Link></li>)
                }
            </div >
        );
    }
};

TracksList.propTypes = {
    tracks: PropTypes.array.isRequired, // this is passed from the Rails view
};

export default TracksList;