import React, { useState, useMemo, useCallback } from 'react';
import axios from 'axios';
import debounce from "lodash/debounce";
import style from 'bundles/TheRoom/App.module.css';
import { createSearchParams, useNavigate, useSearchParams } from "react-router-dom";

const SearchBar = (props) => {
    const [searchParams] = useSearchParams();
    const [searchValue, setSearchValue] = useState(searchParams.get("q") || "");
    const navigate = useNavigate();

    const sendRequest = useCallback((value) => {
        if (!!value) {
            const config = { headers: { 'Content-Type': 'application/json', Accept: "application/json" }, params: { q: value } };
            navigate(
                {
                    pathname: "/tracks",
                    search: createSearchParams({
                        q: value
                    }).toString()
                }
            )
            axios.get('/api/v1/tracks', config)
                .then(function (response) {
                    props.setTracksValue(response.data["tracks"])
                    return response.body
                })
                .catch(function (error) {

                    console.log(error);
                })
        }
    }, []);


    const debouncedSendRequest = useMemo(() => {
        return debounce(sendRequest, 1000);
    }, [sendRequest]);

    const onChange = (e) => {
        const value = e.target.value;
        setSearchValue(value)
        debouncedSendRequest(value);
    };


    return (
        <nav className="navbar-expand-lg bg-dark">
            <div className="container-fluid">
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <form action="/tracks" className="d-flex w-100 justify-content-end" role="search">
                        <label className={style.bright} htmlFor="searchInput"></label>
                        <input id="searchInput" name="q" className="form-control w-50 me-2" type="search" placeholder="song" aria-label="Search" onChange={onChange} value={searchValue} />

                        <button className="btn btn-outline-light" type="submit">Search</button>
                    </form>
                </div>
            </div>
        </nav >
    );
};

export default SearchBar;
