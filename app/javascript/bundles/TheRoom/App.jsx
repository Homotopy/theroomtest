import PropTypes from 'prop-types';
import React, { useState } from 'react';
import "stylesheets/application.scss";
import Routing from 'bundles/TheRoom/routes/main.jsx';
import { BrowserRouter } from 'react-router-dom'
import SearchBar from 'bundles/TheRoom/components/searchbar/main.jsx';

document.addEventListener("turbolinks:load", function () {
  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
  });
});
const App = (props) => {
  const [tracksValue, setTracksValue] = useState(props.tracks || []);

  return (
    <>
      <BrowserRouter>
        <SearchBar setTracksValue={setTracksValue} />
        <div className="container">
          <Routing tracks={tracksValue} track={props.track} />
        </div >
      </BrowserRouter>
    </>
  );
};

// App.propTypes = {
//   tracks: PropTypes.array.isRequired, // this is passed from the Rails view
// };

export default App;
