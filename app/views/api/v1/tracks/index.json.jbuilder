# frozen_string_literal: true

json.array! @api_v1_tracks, partial: 'api_v1_tracks/api_v1_track', as: :api_v1_track
