# frozen_string_literal: true

json.extract! api_v1_track, :id, :created_at, :updated_at
json.url api_v1_track_url(api_v1_track, format: :json)
