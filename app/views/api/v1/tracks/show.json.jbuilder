# frozen_string_literal: true

json.partial! 'api_v1_tracks/api_v1_track', api_v1_track: @api_v1_track
