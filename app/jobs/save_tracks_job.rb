# frozen_string_literal: true

# Async batch save for tracks
class SaveTracksJob
  include SuckerPunch::Job

  def perform(event)
    event = Track.add_pictures(event)
    result = Track.create(event)

    event.each do |track|
      track['picture'].close
    end

    result
  end
end
