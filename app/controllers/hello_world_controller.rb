# frozen_string_literal: true

# Template hellow world controller
class HelloWorldController < ApplicationController
  layout 'hello_world'

  def index
    @hello_world_props = { tracks: JSON.parse(File.read('spec/fixtures/tracks.json')) }
    SaveTracksJob.perform_async(@hello_world_props[:tracks])
  end

  def show
    track = Track.find_by(private_id: params[:id])
    track_hash = track.attributes
    track_hash['picture_path'] = track.picture.url
    @hello_world_show_props = { track: track_hash }
  end
end
