# frozen_string_literal: true

module Api
  module V1
    # Test class for TracksController
    class TracksController < ApplicationController
      layout 'hello_world'
      before_action :set_api_v1_track, only: %i[show edit update destroy]

      # GET /api/v1/tracks or /api/v1/tracks.json
      def index
        @api_v1_tracks = set_api_v1_tracks_render
        respond_to do |format|
          format.json { render json: @api_v1_tracks }
          format.html { render 'tracks/index' }
        end
      end

      # GET /api/v1/tracks/1 or /api/v1/tracks/1.json
      def show
        @api_v1_track = set_api_v1_track_render
        respond_to do |format|
          format.json { render :show, json: @api_v1_track }
          format.html { render 'tracks/show' }
        end
      end

      # GET /api/v1/tracks/new
      def new
        @api_v1_track = Track.new
        respond_to do |format|
          format.html { render :new, status: :unsupported_media_type }
          format.json { render json: @api_v1_track }
        end
      end

      # GET /api/v1/tracks/1/edit
      def edit
        respond_to do |format|
          format.html { render :edit, status: :unsupported_media_type }
          format.json { render json: @api_v1_track }
        end
      end

      # POST /api/v1/tracks or /api/v1/tracks.json
      def create
        set_api_v1_track_from_params

        respond_to do |format|
          if @api_v1_track.save
            format.html { redirect_to @api_v1_track, notice: 'Track was successfully created.' }
            format.json { render json: @api_v1_track.render, status: :created }
          else
            format.html { render :new, status: :unprocessable_entity }
            format.json { render json: @api_v1_track.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /api/v1/tracks/1 or /api/v1/tracks/1.json
      def update
        respond_to do |format|
          if @api_v1_track.update(api_v1_track_params)
            format.html { redirect_to @api_v1_track, notice: 'Track was successfully updated.' }
            format.json { render json: @api_v1_track.render, status: :ok, location: [:api, :v1, @api_v1_track] }
          else
            format.html { render :edit, status: :unprocessable_entity }
            format.json { render json: @api_v1_track.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /api/v1/tracks/1 or /api/v1/tracks/1.json
      def destroy
        @api_v1_track.destroy
        respond_to do |format|
          format.html { redirect_to api_v1_tracks_url, notice: 'Track was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_api_v1_track
        @api_v1_track = Track.find_by(private_id: params[:private_id])
      end

      def set_api_v1_track_from_params
        @api_v1_track = Track.new(api_v1_track_params)
        nil unless api_v1_picture_params && api_v1_picture_params[:picture]
      end

      def api_v1_track_question
        params.fetch(:q, nil)
      end

      # Only allow a list of trusted parameters through.
      def api_v1_track_params
        params.fetch(:track, {}).permit(:artist_name, :name, :private_id, :link, :genres, :rating)
      end

      def api_v1_picture_params
        params.fetch(:track, {}).permit(:picture)
      end

      def set_api_v1_tracks_render
        if api_v1_track_question.present?
          tracks = RSpotify::Track.search(api_v1_track_question)
          @api_v1_tracks = Track.ingest(tracks)
          SaveTracksJob.perform_async(@api_v1_tracks)
          { tracks: @api_v1_tracks }
        else
          { tracks: [] }
        end
      end

      def set_api_v1_track_render
        if @api_v1_track
          track_hash = @api_v1_track.attributes
          track_hash['picture_path'] = @api_v1_track.picture.url
          { track: track_hash }
        else
          { track: {} }
        end
      end
    end
  end
end
