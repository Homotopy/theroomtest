# frozen_string_literal: true

# Track model
class Track < ApplicationRecord
  has_attached_file :picture, styles: { medium: '300x300>', thumb: '100x100>' },
                              default_url: '/images/:style/missing.png'
  validates :name, presence: true
  validates :private_id, presence: true, uniqueness: true
  validates :link, presence: true
  validates :artist_name, presence: true
  validates_attachment :picture, less_than: 1.megabytes
  validates_attachment_content_type :picture, content_type: ['image/jpg', 'image/jpeg', 'image/png']

  def self.ingest(data)
    data.map do |track|
      {
        'name' => track.name,
        'artist_name' => track.artists.first.name,
        'private_id' => track.id,
        'link' => track.external_urls['spotify'],
        'picture' => track.album.images.map { |image| image['url'] }.first
      }
    end
  end

  def self.add_pictures(data)
    data.map do |track|
      track = track.dup
      FileUtils.mkdir_p 'tmp/images'

      path = "tmp/images/#{track['picture'].split('://')[1].gsub('/', '.')}.jpeg"

      save_picture(track['picture'], path)
      track['picture'] = File.open(path)
      track
    end
  end

  def render
    {
      artist_name: artist_name, private_id: private_id, name: name,
      link: link, genres: genres, picture: picture.name
    }
  end

  def self.save_picture(track_url, path)
    URI.open("https://#{track_url.to_s.split('https://')[1]}") do |f|
      File.open(path, 'wb') do |file|
        file.puts f.read
      end
    end
  end

  def to_param
    private_id
  end
end
